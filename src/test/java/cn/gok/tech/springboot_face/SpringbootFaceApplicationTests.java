package cn.gok.tech.springboot_face;

import cn.gok.tech.springboot_face.entriy.Face;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootFaceApplicationTests {

    @Autowired
    private Face face;

    @Test
    public void contextLoads() {
    }

}
