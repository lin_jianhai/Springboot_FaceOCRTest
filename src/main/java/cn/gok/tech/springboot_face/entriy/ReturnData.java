package cn.gok.tech.springboot_face.entriy;

import lombok.Data;

@Data
public class ReturnData {
    private int code;
    private String img;
    private  String data;
    private  String faceData;

}
