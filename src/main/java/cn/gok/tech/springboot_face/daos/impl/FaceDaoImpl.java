package cn.gok.tech.springboot_face.daos.impl;

import cn.gok.tech.springboot_face.daos.FaceDao;
import cn.gok.tech.springboot_face.entriy.Face;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FaceDaoImpl implements FaceDao {
    @Autowired
  private Face face;

    @Override
    public String getFace(String imageBase64) {
        String faceInfo = face.getFaceInfo(imageBase64);
        return faceInfo;
    }
}