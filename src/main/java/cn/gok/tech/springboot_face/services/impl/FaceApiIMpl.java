package cn.gok.tech.springboot_face.services.impl;

import cn.gok.tech.springboot_face.daos.FaceDao;
import cn.gok.tech.springboot_face.services.FaceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FaceApiIMpl implements FaceApi {

    @Autowired
    private FaceDao faceDao;


    @Override
    public String faceInfo(String imageBase64) {

        return faceDao.getFace(imageBase64);
    }
}
