package cn.gok.tech.springboot_face.services;

public interface FaceApi {

    /*
    人脸识别
     */
    public String faceInfo(String imageBase64);
}
