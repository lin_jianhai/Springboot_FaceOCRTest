package cn.gok.tech.springboot_face.controllers;


import cn.gok.tech.springboot_face.entriy.ReturnData;
import cn.gok.tech.springboot_face.services.FaceApi;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;


@Controller
public class FaceController {

@Autowired
private FaceApi faceApi;

@RequestMapping("face")
@ResponseBody
    public String face(HttpServletRequest request){
//    从前端获取的base64的照片
    String base = request.getParameter("base");

    String faceInfo = faceApi.faceInfo(base);
//    System.out.println(faceInfo);

//    HashMap hashMap=new HashMap();
//    hashMap.put("type",1);
//    hashMap.put("data",faceInfo);



    ReturnData returnData=new ReturnData();
    returnData.setCode(1);
//    returnData.setData(imagePath);
    returnData.setFaceData(faceInfo);

//    System.out.println(new Gson().toJson(returnData));

    return  new Gson().toJson(returnData);
}


}
